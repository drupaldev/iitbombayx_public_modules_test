<?php
/**
 * @file
 * Lists available colors and color schemes for the IITBombayX theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'themecolor' => t('Theme Color'),
    'themesubcolor' => t('Theme Color Hover'),
    'pagebackground' => t('Main background'),
    'headerbackground' => t('Header background'),
    'footerbackground' => t('Footer background'),
    'sidebarbackground' => t('Sidebar background'),
    'coursesbackground' => t('Course container background'),
    'contentbackground' => t('Content background'),
    'sidebartitle' =>  t('Sidebar Title color'),
    'text' => t('Text color'),    

  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('iitbombayx (default)'),
      'colors' => [
        'themecolor' => '#ec5a1a',        
        'pagebackground' => '#f8f8f8',
        'headerbackground' => '#fffffe',
        'footerbackground' => '#ecf0f1',
        'sidebarbackground' => '#fbf6ee',
        'sidebartitle' => '#3a3737',
        'coursesbackground' => '#ededed',
        'contentbackground' => '#fefefe',    
        'text' => '#303030',        
      ],
    ],    
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'css/style.css',
  ],

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],

  // Gradient definitions.
  'gradients' => [
    [
      // (x, y, width, height).
      'dimension' => [0, 0, 0, 0],
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => ['top', 'bottom'],
    ],
  ],

  // Preview files.
  'preview_library' => '',
  'preview_html' => '',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
        'logo' => theme_get_setting('logo.url', 'iitbombayx'),
      ],
    ],
  ],
];
