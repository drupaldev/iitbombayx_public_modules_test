<?php
# This file is part of IITBX-Drupal.
#
# IITBX-Drupal is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free 
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# IITBX-Drupal is distributed in the hope that it will be useful,but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
# more details.
#
# You should have received a copy of the GNU General Public License along with
# IITBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

###############################################################################
#                                                                             #
# Purpose: It create custom setting form                                      #
#                                                                             #
# Created by: IITBombayX-Drupal Team                                          #

###############################################################################

	namespace Drupal\customsitesettings\Form;
	use Drupal\Core\Form\ConfigFormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Url;
	use Drupal\Component\Utility\SafeMarkup;	  
	use Drupal\Core\Messenger\MessengerInterface;


	class ConfigForm extends ConfigFormBase {	 
	 // Method that renders form id.
	 public function getFormId() {
	   return 'configform_customsitesettings_form';
	 }

	 // Instead of hook_form.

	 public function buildForm(array $form, FormStateInterface $form_state) {

        $form = parent::buildForm($form, $form_state);     

        $form['edx_site_information'] = [
              '#type' => 'details',
              '#title' => t('Edx Site Settings'),
              '#open' => TRUE,
            ];

        $form['course_about_page_settings'] = [
              '#type' => 'details',
              '#title' => t('Course About Page Settings'),
              '#open' => TRUE,
            ];

        $form['edx_site_information']['edx_site_path'] = array(
                '#type' => 'textfield',
                '#title' => t('Edx Site Path'),
                '#description' => t('This path is used to display course images on search course page.'),
                '#default_value' => \Drupal::config('edx.settings')->get('edx_site_path'),
                '#group' => t('EDX SITE CONFIGURATION')
        );
/*
        $form['edx_site_information']['user_cookie_name'] = array(
                '#type' => 'textfield',
                '#title' => t('User Info Cookie Name'),
                '#description' => t('This Cookie name is used to access logged in user information.'),
                '#default_value' => \Drupal::config('edx.settings')->get('user_cookie_name'),
                '#group' => t('EDX SITE CONFIGURATION')
        );

        $form['edx_site_information']['edx_cookie_name'] = array(
                '#type' => 'textfield',
                '#title' => t('Edx Logged Cookie Name'),
                '#description' => t('This Cookie name is used to access logged info.'),
                '#default_value' => \Drupal::config('edx.settings')->get('edx_cookie_name'),
                '#group' => t('EDX SITE CONFIGURATION')
        ); 
 */
        $form['course_about_page_settings']['label_optional'] = array(
	        '#markup' => '<b>Information will be shown on course page.</b>',               
        );   

        $form['course_about_page_settings']['show_course_number'] = array(
                '#type' => 'checkbox',
                '#title' => t('Course Number'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_course_number')
        );     
         

        $form['course_about_page_settings']['show_classes_start'] = array(
                '#type' => 'checkbox',
                '#title' => t('Classes Start'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_classes_start')
        );

        $form['course_about_page_settings']['show_classes_end'] = array(
                '#type' => 'checkbox',
                '#title' => t('Classes End'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_classes_end')
        );

        $form['course_about_page_settings']['show_course_efforts'] = array(
                '#type' => 'checkbox',
                '#title' => t('Efforts'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_course_efforts')
        );

        $form['course_about_page_settings']['show_course_domain'] = array(
                '#type' => 'checkbox',
                '#title' => t('Domain'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_course_domain')
        );

        $form['course_about_page_settings']['show_course_programme'] = array(
                '#type' => 'checkbox',
                '#title' => t('Programme'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_course_programme')
        );
        $form['course_about_page_settings']['show_course_pacing'] = array(
                '#type' => 'checkbox',
                '#title' => t('Pacing'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_course_pacing')
        );

        $form['course_about_page_settings']['show_course_timeline'] = array(
                '#type' => 'checkbox',
                '#title' => t('Timeline'),
                '#default_value' => \Drupal::config('edx.settings')->get('show_course_timeline')
        );

        $form['course_about_page_settings']['show_course_moocs'] = array(
                  '#type' => 'checkbox',
                  '#title' => t('Types Of Mooc'),
                  '#default_value' => \Drupal::config('edx.settings')->get('show_course_moocs')
        );

         $form['course_about_page_settings']['show_course_reviews'] = array(
                  '#type' => 'checkbox',
                  '#title' => t('Course Reviews'),
                  '#default_value' => \Drupal::config('edx.settings')->get('show_course_reviews')
        );

        $form['course_about_page_settings']['note_optional'] = array(
               '#markup' => '<b>Note :</b> Pleae clear cache after saving configuration. <a target="_blank" href="development/performance">Click here to clear cache.</a> ',
        );

     return $form;	   
  }   


	 // Instead of hook_form_submit.

	 public function submitForm(array &$form, FormStateInterface $form_state) {

          \Drupal::configFactory()->getEditable('edx.settings')
            ->set('edx_site_path', $form_state->getValue('edx_site_path'))
            ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
            ->set('edx_cookie_name', 'edxloggedin')
            ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
            ->set('user_cookie_name', 'edx-user-info')
            ->save();	   

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_number', $form_state->getValue('show_course_number'))
          ->save();
          
          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_classes_start', $form_state->getValue('show_classes_start'))
          ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_classes_end', $form_state->getValue('show_classes_end'))
          ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_efforts', $form_state->getValue('show_course_efforts'))
          ->save();

           \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_domain', $form_state->getValue('show_course_domain'))
          ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_programme', $form_state->getValue('show_course_programme'))
          ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_pacing', $form_state->getValue('show_course_pacing'))
          ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_timeline', $form_state->getValue('show_course_timeline'))
          ->save();

          \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_moocs', $form_state->getValue('show_course_moocs'))
          ->save();

           \Drupal::configFactory()->getEditable('edx.settings')
          ->set('show_course_reviews', $form_state->getValue('show_course_reviews'))
          ->save();

 \Drupal::messenger()->addMessage(t("The configuration options have been saved. <a target='_blank' href='development/performance'>Click here to clear cache.</a>"));

 #         drupal_set_message(t('The configuration options have been saved. <a target="_blank" href="development/performance">Click here to clear cache.</a> '));
	 }


       // An array of names of configuration objects that are available for editing.

      protected function getEditableConfigNames() {
        return ['edx.settings'];
      }  
  
	}
