 function custom_color_scheme_form_submit() {

    $theme = \Drupal::configFactory()->getEditable('system.theme')->get('default');
    $info = color_get_info($theme); 
    $palette =  \Drupal::configFactory()->getEditable('color.theme.'.$theme)->get('palette');



  if (!isset($info)) {
    return;
  }



  $config = \Drupal::configFactory()->getEditable('color.theme.' . $theme);


  

  $file_system = \Drupal::service('file_system');
  // Delete old files.
  $files = $config->get('files');
  if (isset($files)) {
    foreach ($files as $file) {
      @$file_system->unlink($file);
    }
  }
  if (isset($file) && $file = dirname($file)) {
    @\Drupal::service('file_system')->rmdir($file);
  }

  // No change in color config, use the standard theme from color.inc.
  if (implode(',', color_get_palette($theme, TRUE)) == implode(',', $palette)) {
    $config->delete();
    return;
  }

  // Prepare target locations for generated files.
  $id = $theme . '-' . substr(hash('sha256', serialize($palette) . microtime()), 0, 8);
  $paths['color'] = 'public://color';
  $paths['target'] = $paths['color'] . '/' . $id;
  /** @var \Drupal\Core\File\FileSystemInterface $file_system */
  $file_system = \Drupal::service('file_system');
  foreach ($paths as $path) {
    $file_system->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
  }
  $paths['target'] = $paths['target'] . '/';
  $paths['id'] = $id;
  $paths['source'] = drupal_get_path('theme', $theme) . '/';
  $paths['files'] = $paths['map'] = [];

  // Save palette and logo location.
  $config
    ->set('palette', $palette)
    ->set('logo', $paths['target'] . 'logo.svg')
    ->save();

  // Copy over neutral images.
  /** @var \Drupal\Core\File\FileSystemInterface $file_system */
  $file_system = \Drupal::service('file_system');
  foreach ($info['copy'] as $file) {
    $base = $file_system->basename($file);
    $source = $paths['source'] . $file;
    try {
      $filepath = $file_system->copy($source, $paths['target'] . $base);
    }
    catch (FileException $e) {
      $filepath = FALSE;
    }
    $paths['map'][$file] = $base;
    $paths['files'][] = $filepath;
  }

  // Render new images, if image has been provided.
  if (isset($info['base_image'])) {
    _color_render_images($theme, $info, $paths, $palette);
  }

  // Rewrite theme stylesheets.
  $css = [];
  foreach ($info['css'] as $stylesheet) {
    // Build a temporary array with CSS files.
    $files = [];
    if (file_exists($paths['source'] . $stylesheet)) {
      $files[] = $stylesheet;
    }

    foreach ($files as $file) {
      $css_optimizer = new CssOptimizer();
      // Aggregate @imports recursively for each configured top level CSS file
      // without optimization. Aggregation and optimization will be
      // handled by drupal_build_css_cache() only.
      $style = $css_optimizer->loadFile($paths['source'] . $file, FALSE);

      // Return the path to where this CSS file originated from, stripping
      // off the name of the file at the end of the path.
      $css_optimizer->rewriteFileURIBasePath = base_path() . dirname($paths['source'] . $file) . '/';

      // Prefix all paths within this CSS file, ignoring absolute paths.
      $style = preg_replace_callback('/url\([\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\)/i', [$css_optimizer, 'rewriteFileURI'], $style);

      // Rewrite stylesheet with new colors.
      $style = _color_rewrite_stylesheet($theme, $info, $paths, $palette, $style);
      $base_file = $file_system->basename($file);
      $css[] = $paths['target'] . $base_file;
      _color_save_stylesheet($paths['target'] . $base_file, $style, $paths);
    }
  }

  // Maintain list of files.
  $config
    ->set('stylesheets', $css)
    ->set('files', $paths['files'])
    ->save();
}  